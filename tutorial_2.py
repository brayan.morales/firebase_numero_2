import firebase_admin
from firebase_admin import credentials
from firebase_admin import db


from pyfirmata import Arduino, util
from tkinter import *
from PIL import Image
from PIL import ImageTk
import time
cont=0
cont2=0
placa = Arduino ('COM5')
it = util.Iterator(placa)
it.start()
a_0 = placa.get_pin('a:0:i')
led1 = placa.get_pin('d:3:p')
led2 = placa.get_pin('d:5:p')
led3 = placa.get_pin('d:6:p')
led4 = placa.get_pin('d:9:p')
led5 = placa.get_pin('d:10:p')
led6 = placa.get_pin('d:11:p')
time.sleep(0.5)
ventana = Tk()
ventana.geometry('1280x800')
ventana.title("UI para sistemas de control")

# Fetch the service account key JSON file contents
cred = credentials.Certificate('C:/Users/Brayan Daniel/Desktop/tutorial_2/Llave_2/llave-2.json')
# Initialize the app with a service account, granting admin privileges
firebase_admin.initialize_app(cred, {
    'databaseURL': 'https://elementos-c9bd8.firebaseio.com/'
})


marco1 = Frame(ventana, bg="gray", highlightthickness=1, width=1280, height=800, bd= 5)
marco1.place(x = 0,y = 0)
b=Label(marco1,text="")
img = Image.open("C:/Users/Brayan Daniel/Desktop/logousa.png") # abro la imagen
img = img.resize((150,150))
photoImg=  ImageTk.PhotoImage(img)
b.configure(image=photoImg)
b.place(x = 760,y = 20)


cont = db.reference('boton/boton1/valor').get()
print(cont)
cont2 = db.reference('boton/boton2/valor').get()
print(cont2)


def update_label():
    global cont
    cont=cont+1
    ref = db.reference("boton")
    ref.update({
                'boton1': {
                    'valor': cont         
            }
         })                  
    cont_indicador['text']=str(cont)
    
def update_label2():
    global cont2
    cont2=cont2+1
    ref = db.reference("boton")
    ref.update({
                'boton2': {
                    'valor': cont2         
            }
         })                  
    cont_indicador2['text']=str(cont2)
    
  
cont_indicador= Label(marco1, text=str(cont),bg='cadet blue1', font=("Arial Bold", 15), fg="white", width=5)
cont_indicador.place(x=20, y=90)
cont_indicador2= Label(marco1, text=str(cont2),bg='cadet blue2', font=("Arial Bold", 15), fg="white", width=5)
cont_indicador2.place(x=110, y=90)
start_button=Button(marco1,text="cont",command=update_label)
start_button.place(x=20, y=160)
start_button2=Button(marco1,text="cont",command=update_label2)
start_button2.place(x=90, y=160)




ventana.mainloop()
